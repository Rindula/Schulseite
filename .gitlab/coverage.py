import glob
import os

phpFiles = len(glob.glob("**/*.php", recursive=True))
jsFiles = len(glob.glob("**/*.js", recursive=True))
files = len(glob.glob("**/*", recursive=True))

pcnt = (phpFiles + jsFiles)/files*100

print("Code coverage: " + str(pcnt))

# Schulseite
Dies ist der Code für die Schulseite. Hiermit werden die Hausaufgaben, arbeiten und ähnliches angezeigt.

### Buildstatus
[![Pipeline Badge](https://gitlab.com/Rindula/Schulseite/badges/master/pipeline.svg)](https://gitlab.com/Rindula/Schulseite/commits/master)
![Coverage Badge](https://gitlab.com/Rindula/Schulseite/badges/master/coverage.svg?job=coverage)
